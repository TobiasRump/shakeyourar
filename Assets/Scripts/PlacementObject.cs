using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacementObject : MonoBehaviour
{
    [SerializeField]
    private bool isSelected { get; set; }

    public bool IsSelected
    {
        get => isSelected;
        set
        {
            isSelected = value;
        }
    }

    [SerializeField]
    private TextMesh OverlayLabel { get; set; }

    [SerializeField]
    private string OverlayLabelText { get; set; }
}
