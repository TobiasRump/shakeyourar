using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARTrackedImageManager))]
public class PlaceOnImage : MonoBehaviour
{
    private ARTrackedImageManager _arTrackedImageManager;
    public GameObject[] spawnPrefabs;
    private readonly Dictionary<string, GameObject> _instantiatedPrefabs = new();

    void Awake()    => _arTrackedImageManager = GetComponent<ARTrackedImageManager>();
    void OnEnable() => _arTrackedImageManager.trackedImagesChanged += _OnTrackedImageChanged;
    void OnDisable()=> _arTrackedImageManager.trackedImagesChanged -= _OnTrackedImageChanged;
    

    private void _OnTrackedImageChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        //Debug.Log("Brin drin");
        foreach(ARTrackedImage trackedImage in eventArgs.added)
        {

            string imageName = trackedImage.referenceImage.name;
            Debug.Log("Added: " + imageName);


            foreach (var curPrefab in spawnPrefabs)
            {
                Debug.Log("ImageName: " + imageName);
                Debug.Log("PrefabName: " + curPrefab.name);
                if (string.Compare(curPrefab.name, imageName, System.StringComparison.OrdinalIgnoreCase) == 0
                    && !_instantiatedPrefabs.ContainsKey(imageName))
                {
                    GameObject newPrefav = Instantiate(curPrefab, trackedImage.transform);
                    _instantiatedPrefabs[imageName] = newPrefav;
                }
            }
        }

        foreach (ARTrackedImage trackedImage in eventArgs.updated)
        {
            //Debug.Log("Updated");
            _instantiatedPrefabs[trackedImage.referenceImage.name]
                .SetActive(trackedImage.trackingState == TrackingState.Tracking);
            
        } 

        foreach (ARTrackedImage trackedImage in eventArgs.removed)
        {
            //Debug.Log("removed");
            Destroy(_instantiatedPrefabs[trackedImage.referenceImage.name]);
            _instantiatedPrefabs[trackedImage.referenceImage.name]
               .SetActive(false);

        }
    }
}
