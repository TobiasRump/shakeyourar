using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARRaycastManager))]
public class PlacementController : MonoBehaviour
{
    [SerializeField]
    private GameObject placedPrefab;

    [SerializeField]
    private Camera arCamera;


    private Vector2 touchPosition = default;
    private PlacementObject[] placedObject;
    private PlacementObject lastSelectedObject;
    private bool isHoldingTouch;
    private ARRaycastManager arRaycastManager;
    private static List<ARRaycastHit> arRaycastHits = new();

    private void Awake()
    {
        arRaycastManager = GetComponent<ARRaycastManager>();

    }

    private bool isUserTouchingOnScreen(out Touch touch)
    {
        if(Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);
            return true;
        }
        touch = default;
        return false;
    }
   

    // Update is called once per frame
    void Update()
    {
        if (!isUserTouchingOnScreen(out Touch touch)) return;

        touchPosition = touch.position;
        Debug.Log(touch.position);

        if (touch.phase == TouchPhase.Began)
        {
            Ray ray = arCamera.ScreenPointToRay(touch.position);
            RaycastHit hitObject;

            if (Physics.Raycast(ray, out hitObject))
            {
                lastSelectedObject = hitObject.transform.GetComponent<PlacementObject>();
            }
            if (lastSelectedObject != null)
            {
                PlacementObject[] allOtherObjects = FindObjectsOfType<PlacementObject>();
                foreach (PlacementObject obj in allOtherObjects)
                {
                    obj.IsSelected = obj == lastSelectedObject;
                }
            }
        }

        if (touch.phase == TouchPhase.Ended)
        {
            lastSelectedObject.IsSelected = false;
        }


        if (arRaycastManager.Raycast(touchPosition, arRaycastHits, TrackableType.PlaneWithinPolygon))
        {
            Pose hitPose = arRaycastHits[0].pose;
            if (lastSelectedObject == null)
            {
                lastSelectedObject = Instantiate(placedPrefab, hitPose.position, hitPose.rotation).GetComponent<PlacementObject>();
            }
            else
            {
                if (lastSelectedObject.IsSelected)
                {
                    lastSelectedObject.transform.position = hitPose.position;
                    lastSelectedObject.transform.rotation = hitPose.rotation;

                }
            }
        }


    }
}

