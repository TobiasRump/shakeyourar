﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void PlacementController::Awake()
extern void PlacementController_Awake_m30FDECDB403E2B12568E5D8214828729CE30D452 (void);
// 0x00000002 System.Boolean PlacementController::isUserTouchingOnScreen(UnityEngine.Touch&)
extern void PlacementController_isUserTouchingOnScreen_m9BD0144EA365566FF93EDFE2A752FEA0F296CD09 (void);
// 0x00000003 System.Void PlacementController::Update()
extern void PlacementController_Update_m2A9A8C06C56556E710F96B592E5FBAA17898D775 (void);
// 0x00000004 System.Void PlacementController::.ctor()
extern void PlacementController__ctor_m958492B38043C6E15A525ECC2CBF773BA764E823 (void);
// 0x00000005 System.Void PlacementController::.cctor()
extern void PlacementController__cctor_mC1765A93CD024093474D668067D368653DFA9AC2 (void);
// 0x00000006 System.Boolean PlacementObject::get_isSelected()
extern void PlacementObject_get_isSelected_m285C602122FDD7AE1B45664649DA81F0336C31EB (void);
// 0x00000007 System.Void PlacementObject::set_isSelected(System.Boolean)
extern void PlacementObject_set_isSelected_m3481652AEB7797DE9522D87E1CBC4A5C3C05619E (void);
// 0x00000008 System.Boolean PlacementObject::get_IsSelected()
extern void PlacementObject_get_IsSelected_m63B95532F4BDF7B0E36DA226A53D31C46E6686C4 (void);
// 0x00000009 System.Void PlacementObject::set_IsSelected(System.Boolean)
extern void PlacementObject_set_IsSelected_m71D44AA1CD260098F4FDC8CD3660A34BF610926F (void);
// 0x0000000A UnityEngine.TextMesh PlacementObject::get_OverlayLabel()
extern void PlacementObject_get_OverlayLabel_m2D25484D18D8871272D949F31079FCFFBDA787D3 (void);
// 0x0000000B System.Void PlacementObject::set_OverlayLabel(UnityEngine.TextMesh)
extern void PlacementObject_set_OverlayLabel_m4A8FE5426BD695F5BE644B51D9261424DC801429 (void);
// 0x0000000C System.String PlacementObject::get_OverlayLabelText()
extern void PlacementObject_get_OverlayLabelText_mED1A8047D66BB213AEB9C41ED6CD5344A1AE5D6A (void);
// 0x0000000D System.Void PlacementObject::set_OverlayLabelText(System.String)
extern void PlacementObject_set_OverlayLabelText_mC7CE4CD3326C06D925E7EBF07EE49F49C7E834BA (void);
// 0x0000000E System.Void PlacementObject::.ctor()
extern void PlacementObject__ctor_mF8EF055F28601A348C4D90EE79C08501723AF34E (void);
// 0x0000000F System.Void PlaceOnImage::Awake()
extern void PlaceOnImage_Awake_m640506814C0250601A8A284B96B2E59880F0140A (void);
// 0x00000010 System.Void PlaceOnImage::OnEnable()
extern void PlaceOnImage_OnEnable_m106DC1A0DBE0B494F4E2D24FC29802ED6C869D4F (void);
// 0x00000011 System.Void PlaceOnImage::OnDisable()
extern void PlaceOnImage_OnDisable_mE0CD5D154A34952B6469589590E2F459F576AD36 (void);
// 0x00000012 System.Void PlaceOnImage::_OnTrackedImageChanged(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void PlaceOnImage__OnTrackedImageChanged_m5046E07270F2ED6F704ACB9D0BBDA0BF9988282D (void);
// 0x00000013 System.Void PlaceOnImage::.ctor()
extern void PlaceOnImage__ctor_m1A610FE41DB8BDFECD00D19852815362E198B708 (void);
static Il2CppMethodPointer s_methodPointers[19] = 
{
	PlacementController_Awake_m30FDECDB403E2B12568E5D8214828729CE30D452,
	PlacementController_isUserTouchingOnScreen_m9BD0144EA365566FF93EDFE2A752FEA0F296CD09,
	PlacementController_Update_m2A9A8C06C56556E710F96B592E5FBAA17898D775,
	PlacementController__ctor_m958492B38043C6E15A525ECC2CBF773BA764E823,
	PlacementController__cctor_mC1765A93CD024093474D668067D368653DFA9AC2,
	PlacementObject_get_isSelected_m285C602122FDD7AE1B45664649DA81F0336C31EB,
	PlacementObject_set_isSelected_m3481652AEB7797DE9522D87E1CBC4A5C3C05619E,
	PlacementObject_get_IsSelected_m63B95532F4BDF7B0E36DA226A53D31C46E6686C4,
	PlacementObject_set_IsSelected_m71D44AA1CD260098F4FDC8CD3660A34BF610926F,
	PlacementObject_get_OverlayLabel_m2D25484D18D8871272D949F31079FCFFBDA787D3,
	PlacementObject_set_OverlayLabel_m4A8FE5426BD695F5BE644B51D9261424DC801429,
	PlacementObject_get_OverlayLabelText_mED1A8047D66BB213AEB9C41ED6CD5344A1AE5D6A,
	PlacementObject_set_OverlayLabelText_mC7CE4CD3326C06D925E7EBF07EE49F49C7E834BA,
	PlacementObject__ctor_mF8EF055F28601A348C4D90EE79C08501723AF34E,
	PlaceOnImage_Awake_m640506814C0250601A8A284B96B2E59880F0140A,
	PlaceOnImage_OnEnable_m106DC1A0DBE0B494F4E2D24FC29802ED6C869D4F,
	PlaceOnImage_OnDisable_mE0CD5D154A34952B6469589590E2F459F576AD36,
	PlaceOnImage__OnTrackedImageChanged_m5046E07270F2ED6F704ACB9D0BBDA0BF9988282D,
	PlaceOnImage__ctor_m1A610FE41DB8BDFECD00D19852815362E198B708,
};
static const int32_t s_InvokerIndices[19] = 
{
	5289,
	2819,
	5289,
	5289,
	8409,
	5109,
	4135,
	5109,
	4135,
	5194,
	4217,
	5194,
	4217,
	5289,
	5289,
	5289,
	5289,
	4122,
	5289,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	19,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
